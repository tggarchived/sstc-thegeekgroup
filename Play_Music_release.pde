/*
Solid State Tesla Coil Polytonal MIDI/uSD Interrupter Software for Leaflabs Maple processor board.
Written by Paul Kidwell, Chairman of the Board of Directors for The Geek Group. 
Copyright ©2013. The Geek Group (TGG). All Rights Reserved. 

Permission to use, copy, modify, and distribute this software and its documentation for educational, research, and not-for-profit purposes, without fee and without a signed licensing agreement, is hereby granted, provided that the above copyright notice, this paragraph and the following three paragraphs appear in all copies, modifications, and distributions. Contact The Geek Group, 902 Leonard St. NW, Grand Rapids, MI 49504, for commercial licensing opportunities.

IN NO EVENT SHALL TGG BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF TGG HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

TGG SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS IS". TGG HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

This source code is being provided for your use as stated above at no charge. In order for us to continue to bring you Awesome, please consider making a tax deductable donation to The Geek Group at: http://thegeekgroup.org/support/donate/ and thank you for your support.
*/

// use these for all but first two prototype boards
#define RedLED   5
#define GreenLED 6
#define DumpSW   9

// use these for first two prototype boards
//#define RedLED   6
//#define GreenLED 7
//#define DumpSW   5

#define voices  12
#define OutPin   3

#include <SdFat.h>
#include <HardwareSPI.h>
#include <stdint.h>
#include <stdlib.h>

HardwareSPI spi(1);
//Sd2Card card(spi, USE_NSS_PIN, true);
Sd2Card card(spi, 10, true);
SdVolume volume;
SdFile root;
SdFile file;

unsigned long n[voices];
unsigned int p[voices];
unsigned int old[voices];
unsigned long uspn,g,k,l,m,o,q,dur;
byte incomingByte;
byte note;
byte velocity;
int flag2=0;
byte lastcmd=0;
unsigned int h,i,j,d;
char buff[100];
char name[13];
int16_t c;

int ffub;
unsigned int duty;

unsigned long period[] = {
  122312, 115447, 108968, 102852, 97079, 91631, 86488, 81634, 77052, 72727, 68645, 64792,
  61156, 57724, 54484, 51426, 48540, 45815, 43244, 40817, 38526, 36364, 34323, 32396, 
  30578, 28862, 27242, 25713, 24270, 22908, 21622, 20408, 19263, 18182, 17161, 16198, 
  15289, 14431, 13621, 12857, 12135, 11454, 10811, 10204, 9632, 9091, 8581, 8099, 
  7645, 7216, 6811, 6428, 6068, 5727, 5406, 5102, 4816, 4546, 4290, 4050, 
  3822, 3608, 3405, 3214, 3034, 2864, 2703, 2551, 2408, 2273, 2145, 2025, 
  1911, 1804, 1703, 1607, 1517, 1432, 1351, 1276, 1204, 1136, 1073, 1012, 
  956, 902, 851, 804, 758, 716, 676, 638, 602, 568, 536, 506, 
  478, 451, 426, 402, 379, 358, 338, 319, 301, 284, 268, 253, 
  239, 226, 213, 201, 190, 179, 169, 159, 151, 142, 134, 127, 
  119, 113, 106, 100, 95, 90, 85, 80};

// store error strings in flash to save RAM
void error(const char* str) {
  SerialUSB.print("error: ");
  SerialUSB.println(str);
  if (card.errorCode()) {
    SerialUSB.print("SD error: ");
    SerialUSB.print(card.errorCode(), HEX);
    SerialUSB.print(',');
    SerialUSB.println(card.errorData(), HEX);
  }
//  while(1);
}

void keyboard(void) {
  int voice=1;
  SerialUSB.println("Done playing uSD files.\nStarting MIDI keyboard");
  digitalWrite(GreenLED, HIGH);
  digitalWrite(RedLED, LOW);
  duty=1;
  for (i=0;i<voices;i++) p[i]=0;
  while(1) {
    if (Serial2.available() > 0) {
      incomingByte = Serial2.read();
      if(incomingByte & 0x80) {
        digitalWrite(GreenLED, HIGH);
        digitalWrite(RedLED, LOW);
        incomingByte=incomingByte&0xF0;
        k=micros();
        if(incomingByte != 0xF0) lastcmd=incomingByte;
        flag2=0;
      }
      else {
        if(flag2==0){
          note=incomingByte;
          flag2=1;
        }
        else
        {
          flag2=0;
          velocity=incomingByte;
  
  // note off 80
          if(lastcmd==0x80) {
            digitalWrite(OutPin, LOW);
            for (i=0;i<=voices;i++) if(p[i]==note) p[i]=0;
  
            for (i = 1; i < voices; ++i)
            {
              j = p[i];
              for (h = i - 1; (h >= 0) && (j > p[h]); h--)
              {
                p[h + 1] = p[h];
              }
              p[h + 1] = j;
            }

            l=micros();

            voice=1;
            for (i=0;i<=voices;i++)  {
              if (p[i]!=0) {
                n[i]=period[p[i]]+l;
                voice=i+1;
              }
              else i=voices+1;
            }

            q=period[p[0]];
            q=q/voice;
            for (i=0;i<voice;i++) n[i]+=q*i;

            o=l+(period[p[0]]>>duty);
          }
  
  // note on 90
          if(lastcmd==0x90) {
            if(velocity==0) {                                   // This is acutally a note off
              digitalWrite(OutPin, LOW);
              for (i=0;i<=voices;i++) if(p[i]==note) p[i]=0;
            }
            else {                                              // This is a note on
              for (i=0;i<=voices;i++) {
                if(p[i]==0) p[i]=note;
                if(p[i]==note) i=voices+1;
              }
            }

            for (i = 1; i < voices; ++i)
            {
              j = p[i];
              for (h = i - 1; (h >= 0) && (j > p[h]); h--)
              {
                p[h + 1] = p[h];
              }
              p[h + 1] = j;
            }

            l=micros();
            voice=1;
            for (i=0;i<=voices;i++)  {
              if (p[i]!=0) {
                n[i]=period[p[i]]+l;
                voice=i+1;
              }
              else i=voices+1;
            }

            q=period[p[0]];
            q=q/voice;
            for (i=0;i<voice;i++) n[i]+=q*i;

            o=l+(period[p[0]]>>duty);
          }
  
  // AfterTouch A0
  // Control Change B0
  // Program Change C0
  // Channel Pressure D0
  // Pitch Wheel E0

        }
      }
    }
    
    l=micros();
    if((l-k)>500000){
      for (i=0;i<voices;i++) p[i]=0;
      digitalWrite(OutPin, LOW);  
  
      SerialUSB.println("Keyboard Disconnected!!! ");
      digitalWrite(GreenLED, LOW);
      digitalWrite(RedLED, LOW);
    }
  
    m=l+10000000;
    if (l>o) digitalWrite(OutPin, LOW);
    j=0;
    for (i=0;i<voices;i++) {
      if(p[i]==0) i=voices+1;
      else {
        g=period[p[i]];
        if (l>n[i]){
          if (p[i] != 0) digitalWrite(OutPin, HIGH);
          n[i]+=g;
          j++;
        }
        if (m>n[i]) m=n[i];
        if (j!=0) o=l+((m-l)>>duty);
      }
    }   
  }
}

void PlayNote(int a) {
  int voice=1;
  
  if (a==10000) {
    uspn = 3750000/p[0];
    if(p[1]!=0) duty=p[1];
  }
  else {
    l=micros();
    if (p[0] != 0) digitalWrite(OutPin, HIGH);

    if (a>1000) dur=long(((a-1000)*uspn)*2/3);
    else dur=a*uspn;
    k=l+dur;

    voice=1;
    for (i=0;i<voices;i++)  {
      n[i]=period[p[i]]+l;
      if (p[i]!=0) voice=i+1;
    }
    q=period[p[0]];
    q=q/voice;
    for (i=0;i<voice;i++) n[i]+=q*i;

    o=l+(period[p[0]]>>duty);
    
    while(l<k) {
      l=micros();
      m=l+10000000;
      if (l>o) digitalWrite(OutPin, LOW);
      j=0;
      for (i=0;i<voice;i++) {
        g=period[p[i]];
        if (l>n[i]){
          if (p[i] != 0) digitalWrite(OutPin, HIGH);
          n[i]+=g;
          j++;
        }
        if (m>n[i]) m=n[i];
        if (j!=0) o=l+((m-l)>>duty);
      }
    }
    digitalWrite(OutPin, LOW);
  }
}

void setup(void) {
  pinMode(OutPin, OUTPUT);
  pinMode(GreenLED, OUTPUT);
  pinMode(RedLED, OUTPUT);
  pinMode(DumpSW, INPUT_PULLUP);
  pinMode(29, INPUT_PULLUP);
  pinMode(31, INPUT_PULLUP);
  pinMode(33, INPUT_PULLUP);
  pinMode(35, INPUT_PULLUP);
  pinMode(37, INPUT_PULLUP);
  digitalWrite(OutPin, LOW);
  digitalWrite(GreenLED, HIGH);
  digitalWrite(RedLED, HIGH);
  Serial2.begin(31250);
  SerialUSB.begin();
  SerialUSB.println();
  SerialUSB.println("MIDI Receive 2012-08-27");

  // initialize the SD card
  if (!card.init()) error("card.init failed!");
  if (!volume.init(&card,1)) error("vol.init failed!");
  if (!root.openRoot(&volume)) error("openRoot failed");
}

/*
 * Print tail of all SdFat example files
 */
void loop(void) {
  char tempbuf[20];
  dir_t dir;
  
  // read next directory entry
  if (root.readDir(&dir) != sizeof(dir)) keyboard();  
  // format file name
  SdFile::dirName(dir, name);
  
  // remember position in root dir
  uint32_t pos = root.curPosition();
  
  // open file
  if (!file.open(&root, name, O_READ)) error("file.open failed");
  
  // restore root position
  if (!root.seekSet(pos)) error("root.seekSet failed");
  
  // print file name message
  SerialUSB.print("\n\nNow Playing: ");
  SerialUSB.println(name);

  delay(2000);
  digitalWrite(GreenLED, LOW);
  digitalWrite(RedLED, HIGH);
  uspn = 37500;                      // default to 100 quarter notes per minute
  duty=1;
  ffub=0;

  // Play file
  while ((c = file.read()) > 0) {
    if (digitalRead(DumpSW)==LOW) {
      digitalWrite(OutPin, LOW);  
      SerialUSB.println("\nSkipping Song!\n");
      digitalWrite(GreenLED, HIGH);
      digitalWrite(RedLED, HIGH);
      while(digitalRead(DumpSW)==LOW);
      file.close();
      return;
    }

    buff[ffub++]=c;

    if (ffub>55) {
      digitalWrite(OutPin, LOW);  
      SerialUSB.println("\nAborting Song!\n");
      digitalWrite(GreenLED, HIGH);
      digitalWrite(RedLED, HIGH);
      while(digitalRead(DumpSW)==LOW);
      file.close();
      return;
    }

    if(c=='\n'){
      for (i=0;i<voices;i++) p[i]=0;
      buff[ffub]=0;
      
      h=0;
      j=0;
      for (i=0;i<ffub;i++) {
        switch (buff[i]) {
        case '\n':
            tempbuf[h]=0;
            p[j++]=atoi(tempbuf);
            h=0;
            i=ffub+1;
            break;
        case ' ':
            break;
        case ',':
            tempbuf[h]=0;
            p[j++]=atoi(tempbuf);
            h=0;
            break;
        default:
            tempbuf[h++]=buff[i];
        }        
      }
      d=p[0];
      p[0]=0;
      
      if(d!=0) {
        for (i = 0; i < voices-1; ++i) p[i]=p[i+1];
        j=0;
        for (i=0;i<voices;i++) if(old[i]!=p[i])j=1;
        if(j==0) delay(5);
        PlayNote(d);
        for (i=0;i<voices;i++) old[i]=p[i];
      }
      ffub=0;
    }
  }
  file.close();
  digitalWrite(GreenLED, HIGH);
  digitalWrite(RedLED, HIGH);
}


