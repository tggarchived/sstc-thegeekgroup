/*
Solid State Tesla Coil interrupter software to play Deck the Halls in four part harmony.
Written by Paul Kidwell, Chairman of the Board of Directors for The Geek Group. 
Copyright ©2013. The Geek Group (TGG). All Rights Reserved. 

Permission to use, copy, modify, and distribute this software and its documentation for educational, research, and not-for-profit purposes, without fee and without a signed licensing agreement, is hereby granted, provided that the above copyright notice, this paragraph and the following three paragraphs appear in all copies, modifications, and distributions. Contact The Geek Group, 902 Leonard St. NW, Grand Rapids, MI 49504, for commercial licensing opportunities.

IN NO EVENT SHALL TGG BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF TGG HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

TGG SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS IS". TGG HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

This source code is being provided for your use as stated above at no charge. In order for us to continue to bring you Awesome, please consider making a tax deductable donation to The Geek Group at: http://thegeekgroup.org/support/donate/ and thank you for your support.
*/
#define RedLED   5
#define GreenLED 6
#define DumpSW   9
#define OutPin   3

unsigned long k,l,m,o;
unsigned long n[5];
unsigned int  p[70][5];
unsigned int  d[70];
unsigned int g,h,i,j,duty;

unsigned long period[] = {
  122312, 115447, 108968, 102852, 97079, 91631, 86488, 81634, 77052, 72727, 68645, 64792,
  61156, 57724, 54484, 51426, 48540, 45815, 43244, 40817, 38526, 36364, 34323, 32396, 
  30578, 28862, 27242, 25713, 24270, 22908, 21622, 20408, 19263, 18182, 17161, 16198, 
  15289, 14431, 13621, 12857, 12135, 11454, 10811, 10204, 9632, 9091, 8581, 8099, 
  7645, 7216, 6811, 6428, 6068, 5727, 5406, 5102, 4816, 4546, 4290, 4050, 
  3822, 3608, 3405, 3214, 3034, 2864, 2703, 2551, 2408, 2273, 2145, 2025, 
  1911, 1804, 1703, 1607, 1517, 1432, 1351, 1276, 1204, 1136, 1073, 1012, 
  956, 902, 851, 804, 758, 716, 676, 638, 602, 568, 536, 506, 
  478, 451, 426, 402, 379, 358, 338, 319, 301, 284, 268, 253, 
  239, 226, 213, 201, 190, 179, 169, 159, 151, 142, 134, 127, 
  119, 113, 106, 100, 95, 90, 85, 80};

void setup() {                
  Serial.begin(57600);
  pinMode(OutPin, OUTPUT);
  pinMode(RedLED, OUTPUT);
  pinMode(GreenLED, OUTPUT);
  pinMode(DumpSW, INPUT);     
  digitalWrite(DumpSW, HIGH);  
  digitalWrite(OutPin, LOW);

  duty=1;

  p[0][0]=60;
  p[0][1]=57;
  p[0][2]=48;
  p[0][3]=41;
  d[0]=3;
  
  p[1][0]=58;
  p[1][1]=55;
  p[1][2]=48;
  p[1][3]=40;
  d[1]=1;

  p[2][0]=57;
  p[2][1]=53;
  p[2][2]=48;
  p[2][3]=41;
  d[2]=2;

  p[3][0]=55;
  p[3][1]=52;
  p[3][2]=46;
  p[3][3]=36;
  d[3]=2;

  p[4][0]=53;
  p[4][1]=50;
  p[4][2]=45;
  p[4][3]=38;
  d[4]=2;

  p[5][0]=55;
  p[5][1]=52;
  p[5][2]=48;
  p[5][3]=36;
  d[5]=2;

  p[6][0]=57;
  p[6][1]=53;
  p[6][2]=48;
  p[6][3]=41;
  d[6]=2;

  p[7][0]=53;
  p[7][1]=50;
  p[7][2]=45;
  p[7][3]=41;
  d[7]=2;

  p[8][0]=55;
  p[8][1]=52;
  p[8][2]=48;
  p[8][3]=36;
  d[8]=1;

  p[9][0]=57;
  p[9][1]=53;
  p[9][2]=48;
  p[9][3]=36;
  d[9]=1;

  p[10][0]=58;
  p[10][1]=55;
  p[10][2]=48;
  p[10][3]=36;
  d[10]=1;

  p[11][0]=55;
  p[11][1]=52;
  p[11][2]=48;
  p[11][3]=36;
  d[11]=1;

  p[12][0]=57;
  p[12][1]=53;
  p[12][2]=48;
  p[12][3]=41;
  d[12]=3;

  p[13][0]=55;
  p[13][1]=50;
  p[13][2]=46;
  p[13][3]=34;
  d[13]=1;

  p[14][0]=53;
  p[14][1]=48;
  p[14][2]=45;
  p[14][3]=36;
  d[14]=2;

  p[15][0]=52;
  p[15][1]=48;
  p[15][2]=43;
  p[15][3]=36;
  d[15]=2;

  p[16][0]=53;
  p[16][1]=48;
  p[16][2]=45;
  p[16][3]=41;
  d[16]=4;

  p[17][0]=60;
  p[17][1]=57;
  p[17][2]=48;
  p[17][3]=41;
  d[17]=3;
  
  p[18][0]=58;
  p[18][1]=55;
  p[18][2]=48;
  p[18][3]=40;
  d[18]=1;

  p[19][0]=57;
  p[19][1]=53;
  p[19][2]=48;
  p[19][3]=41;
  d[19]=2;

  p[20][0]=55;
  p[20][1]=52;
  p[20][2]=46;
  p[20][3]=36;
  d[20]=2;

  p[21][0]=53;
  p[21][1]=50;
  p[21][2]=45;
  p[21][3]=38;
  d[21]=2;

  p[22][0]=55;
  p[22][1]=52;
  p[22][2]=48;
  p[22][3]=36;
  d[22]=2;

  p[23][0]=57;
  p[23][1]=53;
  p[23][2]=48;
  p[23][3]=41;
  d[23]=2;

  p[24][0]=53;
  p[24][1]=48;
  p[24][2]=45;
  p[24][3]=41;
  d[24]=2;

  p[25][0]=55;
  p[25][1]=52;
  p[25][2]=48;
  p[25][3]=36;
  d[25]=1;

  p[26][0]=57;
  p[26][1]=53;
  p[26][2]=48;
  p[26][3]=36;
  d[26]=1;

  p[27][0]=58;
  p[27][1]=55;
  p[27][2]=48;
  p[27][3]=36;
  d[27]=1;

  p[28][0]=55;
  p[28][1]=52;
  p[28][2]=48;
  p[28][3]=36;
  d[28]=1;

  p[29][0]=57;
  p[29][1]=53;
  p[29][2]=48;
  p[29][3]=41;
  d[29]=3;

  p[30][0]=55;
  p[30][1]=50;
  p[30][2]=46;
  p[30][3]=34;
  d[30]=1;

  p[31][0]=53;
  p[31][1]=48;
  p[31][2]=45;
  p[31][3]=36;
  d[31]=2;

  p[32][0]=52;
  p[32][1]=48;
  p[32][2]=43;
  p[32][3]=36;
  d[32]=2;

  p[33][0]=53;
  p[33][1]=48;
  p[33][2]=45;
  p[33][3]=41;
  d[33]=4;

  p[34][0]=55;
  p[34][1]=52;
  p[34][2]=48;
  p[34][3]=36;
  d[34]=3;
  p[35][0]=57;
  p[35][1]=53;
  p[35][2]=48;
  p[35][3]=36;
  d[35]=1;
  p[36][0]=58;
  p[36][1]=55;
  p[36][2]=48;
  p[36][3]=36;
  d[36]=2;
  p[37][0]=55;
  p[37][1]=52;
  p[37][2]=48;
  p[37][3]=36;
  d[37]=2;
  p[38][0]=57;
  p[38][1]=53;
  p[38][2]=48;
  p[38][3]=41;
  d[38]=3;
  p[39][0]=58;
  p[39][1]=53;
  p[39][2]=43;
  p[39][3]=41;
  d[39]=1;
  p[40][0]=60;
  p[40][1]=53;
  p[40][2]=45;
  p[40][3]=41;
  d[40]=2;
  p[41][0]=55;
  p[41][1]=55;
  p[41][2]=48;
  p[41][3]=40;
  d[41]=2;
  p[42][0]=57;
  p[42][1]=53;
  p[42][2]=48;
  p[42][3]=41;
  d[42]=1;
  p[43][0]=59;
  p[43][1]=53;
  p[43][2]=48;
  p[43][3]=41;
  d[43]=1;
  p[44][0]=60;
  p[44][1]=52;
  p[44][2]=48;
  p[44][3]=45;
  d[44]=2;
  p[45][0]=62;
  p[45][1]=57;
  p[45][2]=48;
  p[45][3]=41;
  d[45]=1;
  p[46][0]=64;
  p[46][1]=57;
  p[46][2]=48;
  p[46][3]=41;
  d[46]=1;
  p[47][0]=65;
  p[47][1]=57;
  p[47][2]=48;
  p[47][3]=38;
  d[47]=2;
  p[48][0]=64;
  p[48][1]=55;
  p[48][2]=48;
  p[48][3]=43;
  d[48]=2;
  p[49][0]=62;
  p[49][1]=53;
  p[49][2]=47;
  p[49][3]=43;
  d[49]=2;
  p[50][0]=60;
  p[50][1]=52;
  p[50][2]=48;
  p[50][3]=36;
  d[50]=4;
  p[51][0]=60;
  p[51][1]=57;
  p[51][2]=48;
  p[51][3]=41;
  d[51]=3;
  p[52][0]=58;
  p[52][1]=55;
  p[52][2]=48;
  p[52][3]=40;
  d[52]=1;
  p[53][0]=57;
  p[53][1]=53;
  p[53][2]=48;
  p[53][3]=41;
  d[53]=2;
  p[54][0]=55;
  p[54][1]=52;
  p[54][2]=46;
  p[54][3]=36;
  d[54]=2;
  p[55][0]=53;
  p[55][1]=50;
  p[55][2]=45;
  p[55][3]=38;
  d[55]=2;
  p[56][0]=55;
  p[56][1]=52;
  p[56][2]=48;
  p[56][3]=36;
  d[56]=2;
  p[57][0]=57;
  p[57][1]=53;
  p[57][2]=48;
  p[57][3]=41;
  d[57]=2;
  p[58][0]=53;
  p[58][1]=50;
  p[58][2]=45;
  p[58][3]=41;
  d[58]=2;
  p[59][0]=62;
  p[59][1]=53;
  p[59][2]=46;
  p[59][3]=46;
  d[59]=1;
  p[60][0]=62;
  p[60][1]=53;
  p[60][2]=46;
  p[60][3]=46;
  d[60]=1;
  p[61][0]=62;
  p[61][1]=53;
  p[61][2]=46;
  p[61][3]=46;
  d[61]=1;
  p[62][0]=62;
  p[62][1]=53;
  p[62][2]=46;
  p[62][3]=46;
  d[62]=1;
  p[63][0]=60;
  p[63][1]=53;
  p[63][2]=48;
  p[63][3]=45;
  d[63]=3;
  p[64][0]=58;
  p[64][1]=55;
  p[64][2]=50;
  p[64][3]=46;
  d[64]=1;
  p[65][0]=57;
  p[65][1]=53;
  p[65][2]=48;
  p[65][3]=48;
  d[65]=2;
  p[66][0]=55;
  p[66][1]=52;
  p[66][2]=46;
  p[66][3]=36;
  d[66]=2;
  p[67][0]=53;
  p[67][1]=53;
  p[67][2]=45;
  p[67][3]=41;
  d[67]=4;

  h=0;
  o=0;
  l=micros();
  n[0]=period[p[0][0]]+l;
  n[1]=period[p[0][1]]+l;
  n[2]=period[p[0][2]]+l;
  n[3]=period[p[0][3]]+l;
  o=l+period[p[h][0]]>>duty;
  k=l+250000*d[h];
}

void loop() {
  l=micros();
  m=l+10000000;
  if (l>o) digitalWrite(OutPin, LOW);
  j=0;
  for (i=0;i<4;i++) {
    g=period[p[h][i]];
    if (l>n[i]){
      if (p[h][i]!=0)digitalWrite(OutPin, HIGH);
      n[i]+=g;
      j++;
    }
    if (n[i]<l) n[i]+=g;
    if (m>n[i]) m=n[i];
    if (j!=0) o=l+(m-l)>>duty;
  }
  if (l>k) {
   h++;
   if (h>67) h=0;
   k+=250000*d[h];
 
   digitalWrite(OutPin, LOW);
   if (h==0) delay(2000);
   else delay(15);
      l=micros();
      n[0]=period[p[h][0]]+l;
      n[1]=period[p[h][1]]+l;
      n[2]=period[p[h][2]]+l;
      n[3]=period[p[h][3]]+l;
      o=l+period[p[h][0]]>>duty;
      k=l+250000*d[h];
  }
  i=digitalRead(DumpSW);
  while (i==0){
	digitalWrite(RedLED, HIGH);
    i=digitalRead(DumpSW);
    Serial.println("Pausing");
    delay(100);
  }
  digitalWrite(RedLED, LOW);
}



