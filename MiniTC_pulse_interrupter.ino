/*
Solid State Tesla Coil Single Tone Interrupter Software for the Arduino 1.0.3 IDE.
Written by Paul Kidwell, Chairman of the Board of Directors for The Geek Group. 
Copyright ©2013. The Geek Group (TGG). All Rights Reserved. 

Permission to use, copy, modify, and distribute this software and its documentation for educational, 
research, and not-for-profit purposes, without fee and without a signed licensing agreement, is hereby 
granted, provided that the above copyright notice, this paragraph and the following three paragraphs 
appear in all copies, modifications, and distributions. Contact The Geek Group, 902 Leonard St. NW, 
Grand Rapids, MI 49504, for commercial licensing opportunities.

IN NO EVENT SHALL TGG BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL 
DAMAGES, INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF 
TGG HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

TGG SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION, 
IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS IS". TGG HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, 
UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

This source code is being provided for your use as stated above at no charge. In order for us to continue 
to bring you Awesome, please consider making a tax deductable donation to The Geek Group at: 
http://thegeekgroup.org/support/donate/ and thank you for your support.
*/

// use these for all but first two prototype boards
#define RedLED   5
#define GreenLED 6
#define DumpSW   9
#define OutPin   3

float freq = 440;          //Change the frequency of the tone here
float period;
float duty = .50;          //Change the duty cycle of the tone here
unsigned long ontime, offtime, start;

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(OutPin, OUTPUT);
  period=1/freq;
  ontime=1000000*period*duty;
  offtime=1000000*period*(1.0-duty);
}

// the loop routine runs over and over again forever:
void loop() {
  start=micros();
  digitalWrite(OutPin, HIGH);   // turn the LED on (HIGH is the voltage level)
  while (micros()<start+ontime);
  digitalWrite(OutPin, LOW);    // turn the LED off by making the voltage LOW
  while (micros()<start+ontime+offtime);
}
