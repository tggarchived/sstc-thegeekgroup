/*
Solid State Tesla Coil Polytonal MIDI/uSD Interrupter Software for Leaflabs Maple processor board.
Written by Paul Kidwell, Chairman of the Board of Directors for The Geek Group. 
Copyright �2013. The Geek Group (TGG). All Rights Reserved. 

Permission to use, copy, modify, and distribute this software and its documentation for educational, research,
and not-for-profit purposes, without fee and without a signed licensing agreement, is hereby granted, provided
that the above copyright notice, this paragraph and the following three paragraphs appear in all copies,
modifications, and distributions. Contact The Geek Group, 902 Leonard St. NW, Grand Rapids, MI 49504, for
commercial licensing opportunities.

IN NO EVENT SHALL TGG BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF TGG HAS BEEN
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

TGG SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS
PROVIDED "AS IS". TGG HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

This source code is being provided for your use as stated above at no charge. In order for us to continue to
bring you Awesome, please consider making a tax deductable donation to The Geek Group at:
http://thegeekgroup.org/support/donate/ and thank you for your support.


--- 2013.04.20 Refactored by Kevin Timmerman ---
- Added comments
- Added conditional compilation for Arduino (atMEGA328p) or Maple
- Split large functions in to smaller functions - eliminated "copy & paste" programming
- Improved speed of synthesis engine by reducing unnecessary work
- Fixed micros() wraparound bugs: for example changed if(a > b) to if(int32_t(a - b) > 0)
- Now uses only standard integer types from <stdint.h>
- Gave some variables more meaningful names
- Reduced use of global variables
- Added proper scope and storage class quailifiers (static, const, ect..)
- MIDI parser will now properly parse all 2 and 3 octet messages
- Music file parser is more efficient, uses less RAM (fewer buffers)
- Store and play music from AVR internal flash memory
*/
                                                //
#define _MAPLE_ 0                               // Set to non-zero only for Maple board
#define SD_FILES 0                              // Play files from SD card
#define FLASH_FILES 1                           // Play files stored in the micrcontroller's flash
                                                //
#define OutPin   3                              // Tesla coil interrupter output pin
#define RedLED   5                              // Red LED output pin
#define GreenLED 6                              // Green LED output pin
#define DumpSW   9                              // Switch input pin
                                                //
#define voices  12                              // Maximum number of concurrent voices
                                                //
static uint_fast8_t voice_midi_note[voices];    // Array of MIDI note numbers; 0 for unused voice / end of active voices
static uint32_t voice_on_time[voices];          // Array of oscillator period start times
static uint_fast8_t duty;                       // Duty cycle: 100 / (2 ^ duty) %
static uint32_t k;                              // MIDI keyboard timeout time
                                                //
                                                //
#if SD_FILES                                    //
#include <stdlib.h>                             // Header files for SD card support
#include <SdFat.h>                              //
#include <HardwareSPI.h>                        //
static HardwareSPI spi(1);                      // Global C++ objects for SD card support
static Sd2Card card(spi, 10, true);             //
static SdVolume volume;                         //
#endif                                          //
                                                //
#if FLASH_FILES                                 //
#include "fake_sd.h"                            // Header file for music in AVR flash memory
#endif                                          //
                                                //
#if SD_FILES || FLASH_FILES                     // Global C++ objects for SD or internal flash playback
static SdFile root;                             //
static SdFile file;                             //
#endif                                          //
                                                //
                                                //
static const uint32_t period[128] = {           // MIDI note period in microseconds
//   C     C#/D-     D     D#/E-    E      F    F#/G-    G    G#/A-    A    A#/B-    B       Octave
  122312, 115447, 108968, 102852, 97079, 91631, 86488, 81634, 77052, 72727, 68645, 64792, // -5
   61156,  57724,  54484,  51426, 48540, 45815, 43244, 40817, 38526, 36364, 34323, 32396, // -4
   30578,  28862,  27242,  25713, 24270, 22908, 21622, 20408, 19263, 18182, 17161, 16198, // -3
   15289,  14431,  13621,  12857, 12135, 11454, 10811, 10204,  9632,  9091,  8581,  8099, // -2
    7645,   7216,   6811,   6428,  6068,  5727,  5406,  5102,  4816,  4546,  4290,  4050, // -1
    3822,   3608,   3405,   3214,  3034,  2864,  2703,  2551,  2408,  2273,  2145,  2025, //  0
    1911,   1804,   1703,   1607,  1517,  1432,  1351,  1276,  1204,  1136,  1073,  1012, //  1
     956,    902,    851,    804,   758,   716,   676,   638,   602,   568,   536,   506, //  2
     478,    451,    426,    402,   379,   358,   338,   319,   301,   284,   268,   253, //  3
     239,    226,    213,    201,   190,   179,   169,   159,   151,   142,   134,   127, //  4
     119,    113,    106,    100,    95,    90,    85,    80                              //  5
};                                              //
                                                //
                                                //
static void init_synth(const bool sort = true)  // --- Initialize the synthesiser ---
                                                // - Sort the voices by MIDI note / frequency
                                                // - Set the inital voice on time
                                                // - Begin synthesis of first voice (highest frequency)
{                                               //
  uint_fast8_t h, i, j;                         // Variables for array iteration
                                                //
  if(sort) {                                    // Sort the MIDI note values
                                                //  This will only be done for MIDI only,
                                                //   text files are expected to be in sorted order
                                                //  This will also remove any 0 entries that
                                                //   may be in the array due to MIDI note off messages
    for(i = 1; i < voices; ++i) {               // Iterate the entire voice_midi_note[] array
      const uint_fast8_t j = voice_midi_note[i];// Get note at index
                                                // Make a hole in the array if necessary to put
                                                //  the arrray in sorted order
      for(h = i; h && (j > voice_midi_note[h - 1]); --h) // While current note is higher
        voice_midi_note[h] = voice_midi_note[h - 1]; // Move entry down
                                                //
      voice_midi_note[h] = j;                   // Fill the hole - put back in the array
    }                                           //
  }                                             //
                                                //
  if(!voice_midi_note[0]) return;               // Exit now if there are no notes to play
                                                //
  digitalWrite(OutPin, HIGH);                   // Turn on interrupter - begin synthesis right now
                                                //
  const uint32_t t = micros();                  // Get current time
                                                //
                                                // Initialize the voice on time to the current time
                                                //  plus the voice period
  for(i = 0; (i < voices) && voice_midi_note[i]; ++i)
      voice_on_time[i] = t + period[voice_midi_note[i]];
                                                //
                                                // Space the voices by the period of the highest
                                                //  frequency note divided by the number of active voices
  const uint32_t q = period[voice_midi_note[0]] / i;
                                                //
  for(j = 0; j < i; ++j)                        // Iterate active voices
    voice_on_time[j] += (q * j);                // Add (spacing * index) to voice on time
                                                //
                                                // Determine off time for the first voice (currently playing)
  const uint32_t o = t + (period[voice_midi_note[0]] >> duty);
  while(int32_t(micros() - o) < 0);             // Wait for the off time
  digitalWrite(OutPin, LOW);                    // Turn off interrupter
}                                               //
                                                //
static inline void synth(void)                  // --- Synthesis Engine ---
{                                               // - This will generate one pulse when it is time to do so
                                                // - The interrupter will always be turned off if it has been
                                                //    turned on (no need to turn it off elsewhere in the code)
  const uint32_t t = micros();                  // Get current time
  uint32_t m = t + 0x10000000UL;                // Init next voice time
  uint_fast8_t j = 0;                           // No voices on yet
                                                // - Iterate all active voices
  for(uint_fast8_t i = 0; (i < voices) && voice_midi_note[i]; ++i) {
                                                //
    if(int32_t(t - voice_on_time[i]) >= 0){     // Check if it is time to turn on this voice
      if(!j) digitalWrite(OutPin, HIGH);        // Turn on the interrupter if it is not already on
      ++j;                                      // Increment voice on count
                                                // Add period to on time (make it oscillate)
      voice_on_time[i] += period[voice_midi_note[i]];
    }                                           // Check if this is next voice to turn on, save if so
    if(int32_t(voice_on_time[i] - m) < 0) m = voice_on_time[i];
  }                                             //
  if(j) {                                       // - If the interrupter was turned on, then it must be turned back off
    const uint32_t o = t + ((m - t) >> duty);   // Calculate the off time
    while(int32_t(micros() - o) < 0);           // Wait for the off time
    digitalWrite(OutPin, LOW);                  // Turn the interrupter off
  }                                             //
                                                //
  if(int32_t(t - k) > 0) {                      // - Check for MIDI timeout
    all_notes_off();                            // Turn off all notes
#if _MAPLE_                                     // Send message to serial monitor
    SerialUSB.println("Keyboard Disconnected!!! ");
#endif                                          //
    digitalWrite(GreenLED, LOW);                // Indicate MIDI timeout on LEDs
    digitalWrite(RedLED, LOW);                  //
  }                                             //
}                                               //
                                                //
static void note_off(uint_fast8_t note)         // --- Remove note from active voices ---
{                                               //
  for(uint_fast8_t i = 0; i < voices; ++i) {    // Iterate the entire voice array
    if(voice_midi_note[i] == note) {            // Check if note matches
      voice_midi_note[i] = 0;                   // Set it to zero
      init_synth();                             // Initialize the synth engine - this will remove
                                                //  the zero from the array by sorting
      return;                                   // Note has been removed, so return to caller
    }                                           //
  }                                             //
}                                               //
                                                //
static void all_notes_off(void)                 // --- Turn off all voices ---
{                                               // Clear the array of MIDI notes
  memset(voice_midi_note, 0, sizeof(voice_midi_note));
}                                               //
                                                //
static void note_on(uint_fast8_t note)          // --- Add note to active voices ---
{                                               //
  for(uint_fast8_t i = 0; i < voices; ++i) {    // Iterate the entire voice array
    if(voice_midi_note[i] == 0) {               // Is entry empty?
      voice_midi_note[i] = note;                // Put the new note there
      init_synth();                             // Sort the array and begin synthesis
      return;                                   // Note has been added, return to caller
    } else if(voice_midi_note[i] == note)       // Check if note is already in array
      return;                                   // Don't allow dupes - return to caller
  }                                             //
}                                               //
                                                //
static void parse_midi(uint_fast8_t midi_rx)    // --- Parse MIDI stream ---
{                                               //
  static uint_fast8_t midi_cmd = 0;             // MIDI command
  static uint_fast8_t cmd_len = 0;              // MIDI command length
  static uint_fast8_t octet_count = 0;          // MIDI octets received so far
  static uint_fast8_t note;                     // MIDI note (second octet of command)
                                                //
  if(midi_rx & 0x80) {                          // - Check if start of MIDI message
    digitalWrite(GreenLED, HIGH);               // Indicate MIDI is active on LEDs
    digitalWrite(RedLED, LOW);                  //
    if(midi_rx > 0xF3) return;                  // Ignore system real time and sysex end messages
    k = micros() + 500000UL;                    // Reset MIDI timeout
    midi_cmd = midi_rx;                         // Save first octet as MIDI command
    octet_count = 0;                            // Reset octet received count
                                                // Determine command length from command octet
    cmd_len = ((midi_cmd >= 0xC0 && midi_cmd <=0xDF) || (midi_cmd >= 0xF0 && midi_cmd != 0xF2)) ? 1 : 2;
  } else {                                      // - MIDI message data octet
    ++octet_count;                              // Increment octet count
    if(octet_count == 1) note = midi_rx;        // Save second octet for later
    if(octet_count == cmd_len) {                // Check if command is complete - all octects received
      octet_count = 0;                          // Reset octet count to allow "running status"
      switch(midi_cmd & 0xF0) {                 // Handle command
        case 0x80:                              // Note off
          note_off(note);                       //
          break;                                //
        case 0x90:                              // Note on
          if(midi_rx) {                         //
            note_on(note);                      //
          } else {                              //
            note_off(note);                     //
          }                                     //
          break;                                //
        case 0xA0:                              // AfterTouch
          break;                                //
        case 0xB0:                              // Control Change
          if(note >= 120 && note <= 127)        //
            all_notes_off();                    //
          break;                                //
        case 0xC0:                              // Program Change
          break;                                //
        case 0xD0:                              // Channel Pressure
          break;                                //
        case 0xE0:                              // Pitch Wheel
          break;                                //
        case 0xF0:                              // System common
          break;                                //
      }                                         //
    }                                           //
  }                                             //
}                                               //
                                                //
static void keyboard(void) {                    // --- Play live MIDI ---
#if _MAPLE_                                     //
  SerialUSB.println("Done playing uSD files.\nStarting MIDI keyboard");
#endif                                          //
  digitalWrite(GreenLED, HIGH);                 // Indicate MIDI mode acive on LEDs
  digitalWrite(RedLED, LOW);                    //
  duty = 1;                                     //
                                                //
  all_notes_off();                              // Begin will all voices off
                                                //
  for(;;) {                                     // Forever
#if _MAPLE_                                     //
    int midi_rx = Serial2.read();               // Try to read a char from MIDI serial port
#else                                           //
    int midi_rx = Serial.read();                //
#endif                                          //
    if(midi_rx != -1)                           // If a char was received, then parse it
      parse_midi(midi_rx);                      //
                                                //
    synth();                                    // Do synthesis
  }                                             //
}                                               //
                                                //
static void PlayNote(const uint16_t dur) {      // --- Play voices for a specified duration ---
  static uint32_t uspn;                         // Microseconds (us) per 1/64th note (pn)
                                                //
  if(dur == 10000) {                            // - If the duration is 10000, then setup speed
                                                //  and synthesis duty cycle
                                                //
    if(voice_midi_note[0]) {                    // Setup note timing - beats per minute
                                                // Microsecond timer (1,000,000 ticks per second)
                                                //   * 60 seconds per minute
                                                //  / 16 units in quarter note (beat)
      uspn = (1000000UL * 60 / 16) / voice_midi_note[0];
                                                //
      if(voice_midi_note[1])                    // Setup duty cycle
        duty = voice_midi_note[1];              //
    }                                           //
  } else {                                      // - else play the voices
                                                // Calculate end time
    const uint32_t e = micros() + ((dur <= 1000) ? (uspn * dur) : ((uspn * (dur - 1000) * 2) / 3));
    k = e + 0x10000000UL;                       // Prevent MIDI timeout
    init_synth(false);                          // Initialize synthesis engine
    while(micros() < e) synth();                // Do synthesis for duration
  }                                             //
}                                               //
                                                //
#if _MAPLE_                                     // --- Print error message to serial monitor ---
static void error(const char* str) {            //
  SerialUSB.print("error: ");                   //
  SerialUSB.println(str);                       //
  if(card.errorCode()) {                        //
    SerialUSB.print("SD error: ");              //
    SerialUSB.print(card.errorCode(), HEX);     //
    SerialUSB.print(',');                       //
    SerialUSB.println(card.errorData(), HEX);   //
  }                                             //
}                                               //
#else                                           //
static void error(const char *s) { };           //
#endif                                          //
                                                //
                                                //
void setup(void) {                              // --- Setup ---
  pinMode(OutPin, OUTPUT);                      // Interrupter output
  digitalWrite(OutPin, LOW);                    //
  pinMode(GreenLED, OUTPUT);                    // Green LED
  digitalWrite(GreenLED, HIGH);                 //
  pinMode(RedLED, OUTPUT);                      // Red LED
  digitalWrite(RedLED, HIGH);                   //
  pinMode(DumpSW, INPUT_PULLUP);                // Switch
#if _MAPLE_                                     //
  pinMode(29, INPUT_PULLUP);                    // SD Card (SPI)
  pinMode(31, INPUT_PULLUP);                    //
  pinMode(33, INPUT_PULLUP);                    //
  pinMode(35, INPUT_PULLUP);                    //
  pinMode(37, INPUT_PULLUP);                    //
  SerialUSB.begin();                            // Serial
  SerialUSB.println();                          //
  SerialUSB.println("MIDI Receive 2012-08-27"); //
  Serial2.begin(31250);                         //
#else                                           //
                                                /// todo: setup pins for SD card
//  Serial.begin(31250);                        // Serial
  Serial.begin(9600);                           //
#endif                                          //
#if SD_FILES                                    //
                                                // Initialize the SD card
  if (!card.init()) error("card.init failed!"); //
                                                //
  if (!volume.init(&card, 1)) error("vol.init failed!");
                                                //
  if (!root.openRoot(&volume)) error("openRoot failed");
#endif                                          //
}                                               //

void test(void)
{
  k = 4000000UL;
  duty = 1;
  all_notes_off();
  voice_midi_note[0] = 42;
  init_synth();
  for(;;) synth();
}
                                                //
void loop(void)                                 // --- Play files on SD card ---
{                                               //
  //test();
  dir_t dir;                                    // Read next directory entry
  if (root.readDir(&dir) != sizeof(dir)) keyboard();  
  char name[13];                                // Format file name
  SdFile::dirName(dir, name);                   //
  const uint32_t pos = root.curPosition();      // Remember position in root dir
                                                // Open file
  if (!file.open(&root, name, O_READ)) error("file.open failed");
                                                // Restore root position
  if (!root.seekSet(pos)) error("root.seekSet failed"); 
                                                //
#if _MAPLE_                                     // Print file name message
  SerialUSB.print("\n\nNow Playing: ");         //
  SerialUSB.println(name);                      //
#endif                                          //
                                                //
  delay(2000);                                  // 2 seconds between songs
  digitalWrite(GreenLED, LOW);                  // Indicate SD card playing on LEDs
  digitalWrite(RedLED, HIGH);                   //
  voice_midi_note[0] = 100;                     // Default to 100 quarter notes per minute
  voice_midi_note[1] = 1;                       // Default to 50% duty cycle
  PlayNote(10000);                              // Set defaults
                                                //
  int c;                                        // -- Play file
  uint_fast16_t n = 0;                          // Integer parsing
  int_fast8_t i = -1;                           // Parsed value index (-1 = duration, 0+ = voice_midi_note[i])
  uint_fast16_t duration = 0;                   // Duration (first parsed item)
  uint_fast8_t changed = 0;                     // Item change count
  while((c = file.read()) > 0) {                // While there is file data, get a char
    if(digitalRead(DumpSW) == LOW) {            // Check if switch is pressed, abort playback of this
#if _MAPLE_                                     //  file if it is
      SerialUSB.println("\nSkipping Song!\n");  //
#endif                                          //
      digitalWrite(GreenLED, HIGH);             // Indicate switch press detected
      digitalWrite(RedLED, HIGH);               //
      while(digitalRead(DumpSW) == LOW);        // Wait for switch releast
      file.close();                             // Close file
      return;                                   // Playback aborted
    }                                           //
                                                //
    if(c >= '0' && c <= '9') {                  // Check if numberic digit
      n = (n << 3) + (n << 1) + (c - '0');      // Parse it: n = (n * 10) + new_digit
    } else if(c == ',' || c == '\n') {          // Check if septerator or end of line
      if(i < 0) {                               // Save first value in duration varialbe
        duration = n;                           //
      } else {                                  // Save other values in voice_midi_note[] array
        if(voice_midi_note[i] != n) ++changed;  // Check if this value is different from last line,
                                                //  increment changed count if so
        voice_midi_note[i] = n;                 // Save parsed value
      }                                         //
      n = 0;                                    // Reset parsed value to zero
      ++i;                                      // Increment array index
      if(c == '\n') {                           // - Check if end of line
        if(i < voices) {                        // Add zero terminator to array if there is room
          if(voice_midi_note[i]) ++changed;     //
          voice_midi_note[i] = 0;               //
        }                                       //
        if(!changed) delay(5);                  // Delay for 5 ms if there was no change of notes
                                                //  from previous line
        changed = 0;                            // Reset changed count
        if(i >= 0) PlayNote(duration);          // Do synthesis if not an empty line
        i = -1;                                 // Reset array index
      } else {                                  // - Seperator
        if(i >= voices) {                       // Check if all voices have been parsed and about if so
#if _MAPLE_                                     // Send message to serial monitor
          SerialUSB.println("\nAborting Song!\n");
#endif                                          //
          digitalWrite(GreenLED, HIGH);         // Indicate abort on LEDs
          digitalWrite(RedLED, HIGH);           //
          file.close();                         // Close file
          return;                               // Playback aborted
        }                                       //
      }                                         //
    }                                           //
  }                                             //
  file.close();                                 // Close file
  digitalWrite(GreenLED, HIGH);                 // Indicate playback complete on LEDs
  digitalWrite(RedLED, HIGH);                   //
}                                               //